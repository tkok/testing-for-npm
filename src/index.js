import hello from './hello'

const TestingOutput = {
  outHello: () => {
    return hello();
  },
  outOrigi: arg => {
    console.log(arg);
  }
}
export default TestingOutput
