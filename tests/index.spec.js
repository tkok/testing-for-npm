import TestingOutput from '../src/'
describe('new tests', () => {

  test('first', () => {
    expect(1).toBe(1);
  })

  test('import package', ()=>{
    //console.log(TestingOutput);
    let h = TestingOutput.outHello();
    expect(h).toBe('hello');
    TestingOutput.outOrigi('Good!');
  })

})